<?php
    
    require ('animal.php');
    require ('frog&ape.php');
    $sheep = new Animal("shaun");

    echo $sheep->name ."<br>"; // "shaun"
    echo $sheep->legs ."<br>"; // 2
    var_dump($sheep->cold_blooded);// false
    echo "<br>";
    $sungokong = new Ape("kera sakti");
    $sungokong->yell(); // "Auooo"

    $kodok = new Frog("buduk");
    $kodok->jump() ; // "hop hop"


?>