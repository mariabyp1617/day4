<?php
function tukar_besar_kecil($string)
{
    for($i=0;$i<strlen($string);$i++)
    {
        $uppercase = preg_match('@[A-Z]@', $string[$i]);
        $lowercase = preg_match('@[a-z]@', $string[$i]);
        if($uppercase)
        {
            echo strtolower($string[$i]);
        }
        else if($lowercase)
        {
            echo strtoupper($string[$i]);
        }
        else
        {
            echo $string[$i];
        }
        
    }
    echo "<br>";
//kode di sini
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>